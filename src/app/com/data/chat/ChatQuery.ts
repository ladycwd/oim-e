export default class ChatQuery {
    public text: string = '';
    public startDateTime: string = '';
    public endDateTime: string = '';
}
